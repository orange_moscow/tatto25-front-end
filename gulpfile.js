'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    connect = require('gulp-connect'),
    opn = require('opn');

var importCss = require('gulp-import-css');

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/',
        static_css: '../tattoo/tattoo/raw_static/build/css',
        static_js: '../tattoo/tattoo/raw_static/build/js',
        images: 'build/img'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
        fancy: 'src/js/fancybox.js',//В стилях и скриптах нам понадобятся только main файлы
        style: 'src/css/main.scss',
        bootstrap: 'src/css/bootstrap.less',
        components: 'src/css/components.css',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*',
        images: 'src/img/*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/css/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var server = {
    host: '192.168.0.68',
    port: '9000'
};

gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html))
        .pipe(connect.reload()); //И перезагрузим наш сервер для обновлений
});

gulp.task('scripts', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(gulp.dest(path.build.static_js)) //Выплюнем готовый файл в build
        .pipe(connect.reload()); //И перезагрузим сервер
});

gulp.task('style', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(importCss())
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(gulp.dest(path.build.static_css))//Выплюнем их в папку build
        .pipe(connect.reload());
});
gulp.task('bootstrap', function () {
    gulp.src(path.src.bootstrap) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(less()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(gulp.dest(path.build.static_css))//Выплюнем их в папку build
        .pipe(connect.reload());
});
gulp.task('components', function () {
    gulp.src(path.src.components) //Выберем наш main.scss
        .pipe(rigger())
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(gulp.dest(path.build.static_css))//Выплюнем их в папку build
        .pipe(connect.reload());
});
gulp.task('img', function() {
    gulp.src(path.src.images)
        .pipe(gulp.dest(path.build.images))
});
gulp.task('fonts', function() {
    gulp.src('bower_components/fontawesome/fonts/*.*')
        .pipe(gulp.dest('build/fonts'))
});
gulp.task('icons', function() {
    gulp.src('src/icons/*.*')
        .pipe(gulp.dest('build/icons'))
});

gulp.task('build', [
    //'html:build',
    'scripts',
    'style',
    'img',
    'bootstrap',
    'fonts',
    'icons'
    //'image:build'
]);

gulp.task('watch', function(){
    //watch([path.watch.html], function(event, cb) {
    //    gulp.start('html:build');
    //});
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    //watch([path.watch.img], function(event, cb) {
    //    gulp.start('image:build');
    //});
    //watch([path.watch.fonts], function(event, cb) {
    //    gulp.start('fonts:build');
    //});
});

gulp.task('runserver', function() {
    connect.server({
        host: server.host,
        port: server.port,
        livereload: true
    });
});