# Дизайн для Tattoo25

Проект фронт-энд дизайна для сайта тату-студии

### Версия
0.0.1

### Используемые технологии:
* [node.js] - для установки серверных пакетов
* [Bower] - для установки исходных пакетов для последующей сборки
* [Gulp] - сборка и минификация стилей и скриптов
* [Twitter Bootstrap] - для адаптивной верстки
* [jQuery]

### Для сборки выполнить следующие команды:

Для установки пакетов Node:

```sh
$ sudo npm install
```
Для установки пакетов для последующей сборки
```sh
$ bower install
```
Для сборки пакетов менеджером [Gulp]
```sh
$ gulp build
```
[node.js]:http://nodejs.org
[Twitter Bootstrap]:http://getbootstrap.com/
[jQuery]:http://jquery.com
[Gulp]:http://gulpjs.com
[Bower]:http://bower.io
