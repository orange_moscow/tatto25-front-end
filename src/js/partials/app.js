//= ../../../bower_components/jquery/dist/jquery.js
//= ../../../bower_components/bootstrap/js/collapse.js
//= ../../../bower_components/jquery-backstretch/jquery.backstretch.min.js
//= ../../../bower_components/owlcarousel/owl-carousel/owl.carousel.min.js
$(document).ready(function () {
    $('#main_slider').owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        margin: 10,
        responsiveClass: true,
        singleItem: true,
        autoPlay: 5000,
        navigation: true,
        navigationText: ['Предыдущий', 'Следующий']
    });
    $('#bottom-picture').backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
    $('#contact-form-picture').backstretch("https://s-media-cache-ak0.pinimg.com/originals/6b/cf/70/6bcf7072b65febb71f42739916e6b99a.jpg");

    $(".work-image").fancybox({
        helpers: {
            title: {
                type: 'inside'
            },
            thumbs: {
                width: 50,
                height: 50
            },
            overlay : {
                css : {
                    'background' : 'rgba(58, 42, 45, 0.95)'
                }
            }
            //buttons: {}
        }
    });

});

